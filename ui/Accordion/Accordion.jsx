/* eslint-disable react/prop-types */
/* eslint-disable react/display-name */
import React from "react";
import * as Accordion from "@radix-ui/react-accordion";
import classNames from "classnames";
import { ChevronDownIcon } from "@radix-ui/react-icons";
import "./styles.css";

function AccordionOne() {
  const data = [
    {
      id: 1,
      trigger: "What is your favorite color?",
      content: "My favorite color is blue.",
    },
    {
      id: 2,
      trigger: "What is your favorite food?",
      content: "My favorite food is pizza.",
    },
    {
      id: 3,
      trigger: "What is your favorite hobby?",
      content: "My favorite hobby is reading books.",
    },
    {
      id: 4,
      trigger: "What is your favorite movie?",
      content: "My favorite movie is The Godfather.",
    },
    {
      id: 5,
      trigger: "What is your favorite animal?",
      content: "My favorite animal is a dog.",
    },
  ];

  return (
    <>
      <Accordion.Root
        className="AccordionRoot"
        type="single"
        defaultValue={1}
        collapsible
        // value={value}
        // onValueChange={setValue}
      >
        {data.map((item) => (
          <Accordion.Item
            className="AccordionItem"
            value={item.id}
            key={item.id}
          >
            <AccordionTrigger>{item.trigger}</AccordionTrigger>
            <AccordionContent>{item.content}</AccordionContent>
          </Accordion.Item>
        ))}
      </Accordion.Root>
    </>
  );
}

const AccordionTrigger = React.forwardRef(
  ({ children, className, ...props }, forwardedRef) => (
    <Accordion.Header className="AccordionHeader">
      <Accordion.Trigger
        className={classNames("AccordionTrigger", className)}
        {...props}
        ref={forwardedRef}
      >
        {children}
        <ChevronDownIcon className="AccordionChevron" aria-hidden />
      </Accordion.Trigger>
    </Accordion.Header>
  )
);

const AccordionContent = React.forwardRef(
  ({ children, className, ...props }, forwardedRef) => (
    <Accordion.Content
      className={classNames("AccordionContent", className)}
      {...props}
      ref={forwardedRef}
    >
      <div className="AccordionContentText">{children}</div>
    </Accordion.Content>
  )
);

export default AccordionOne;
